﻿Thank you for considering my application to Blizzard!

To open the source code, make sure that Visual Studio 12/2013 (standard or
express) is installed and run checkers_lite.sln.cmd. It will use the included
CMake binary to generate and open project files.

A note about CMake: When the solution is first opened, the ALL_BUILD project
will be the default. Right click on the "checkers_lite" project and select
"Set as StartUp Project" before running the built exe.

Alternatively, just run the included checkers_lite.exe. Doing so may require
installing the Visual Studio 2013 x86 Redistributable first. These are included
in the "redist" directory.

Mike Eheler
mike.eheler@gmail.com
310-902-5974

﻿#include "game.h"
#include <iostream>
#include <sstream>

#if defined(_WIN32)
#include <windows.h>
#endif // _WIN32

int main(int argc, char * argv[]) {
  try {
    Game game;
    game.Initialize(argc, argv);
    game.Run();
  } catch (std::exception const & e) {
    std::stringstream err_ss;
    err_ss << "ERROR: Unhandled exception: " << e.what() << '\n';
    std::cerr << err_ss.str();
    #if defined(_WIN32)
    OutputDebugStringA(err_ss.str().c_str());
    #endif // _WIN32
  } catch (...) {
    std::cerr << "ERROR: Unhandled unknown exception.\n";
    #if defined(_WIN32)
    OutputDebugStringA("ERROR: Unhandled unknown exception.\n");
    #endif // _WIN32
  }
  return 0;
}

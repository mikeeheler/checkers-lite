﻿#pragma once
#ifndef CHECKERS_LITE_BOARD_H_
#define CHECKERS_LITE_BOARD_H_ 1

#include <cstdint>
#include <stack>
#include "types.h"

// All square references are 1-based and conform to standard checkers notation.
// Dark side are squares 1-12, white side are squares 21-32.

enum SquareType {
  kEmptySquare,
  kDarkSquare,
  kLightSquare
};

struct MoveType {
  uint8_t from;
  uint8_t to;
};

class Board {
 public:
  Board();
  Board(Board const &);
  ~Board();

  void Reset();

  SquareType GetSquareAt(uint8_t square) const;
  void SetSquareAt(uint8_t square, SquareType to);
  uint8_t GetSquareCount(SquareType type) const;
  uint8_t GetStuckSquareCount(SquareType type) const;

  void MakeMove(MoveType move, uint32_t & dark_captured, uint32_t & light_captured);
  void UndoMove();

  bool IsMoveLegal(MoveType move) const;
  bool IsJump(MoveType move) const;

  std::vector<MoveType> GetAllAvailableMoves(SquareType player) const;
  std::vector<MoveType> GetAvailableMoves(uint8_t square) const;
  uint8_t GetForwardProgessCount(SquareType player) const;
  uint8_t GetEdgePieceCount(SquareType player) const;

  void SetBoard(vector<uint8_t> squares);

  Board & operator=(Board const &);

 private:
  Board(Board &&) = delete;
  Board & operator=(Board &&) = delete;

  SquareType board_[32];
  std::stack<unique_ptr<Board>> undo_;
};

#endif // CHECKERS_LITE_BOARD_H_

﻿#include "board.h"
#include <algorithm>
#include "types.h"
#include "util.h"

#define CHECK_SQUARE(square) assert(square >= 1 && square <= 32)

using ::std::vector;

namespace {

#pragma region Look-up tables
// Look-up tables for move validation
bool g_edge[32] = {
  true, true, true, true,
  true, false, false, false,
  false, false, false, true,
  true, false, false, false,
  false, false, false, true,
  true, false, false, false,
  false, false, false, true,
  true, true, true, true
};

// Up 3 = dark advance right from odd rows
bool g_up_3[32] = {
  false, false, false, false,
  false, true, true, true, 
  false, false, false, false,
  false, true, true, true, 
  false, false, false, false,
  false, true, true, true, 
  false, false, false, false,
  false, false, false, false
};

// Up 5 = dark advance left from even rows
bool g_up_5[32] = {
  true, true, true, false,
  false, false, false, false,
  true, true, true, false,
  false, false, false, false,
  true, true, true, false,
  false, false, false, false,
  true, true, true, false,
  false, false, false, false
};

// Up 7 = dark jump right from any row
bool g_up_7[32] = {
  false, true, true, true,
  false, true, true, true,
  false, true, true, true,
  false, true, true, true,
  false, true, true, true,
  false, true, true, true,
  false, false, false, false,
  false, false, false, false
};

// Up 9 = dark jump left from any row
bool g_up_9[32] = {
  true, true, true, false,
  true, true, true, false,
  true, true, true, false,
  true, true, true, false,
  true, true, true, false,
  true, true, true, false,
  false, false, false, false,
  false, false, false, false
};

// Down 3 = light advance right from even rows
bool g_down_3[32] = {
  false, false, false, false,
  false, false, false, false,
  true, true, true, false,
  false, false, false, false,
  true, true, true, false,
  false, false, false, false,
  true, true, true, false,
  false, false, false, false
};

// Down 5 = light advance left from odd rows
bool g_down_5[32] = {
  false, false, false, false,
  false, true, true, true,
  false, false, false, false,
  false, true, true, true,
  false, false, false, false,
  false, true, true, true,
  false, false, false, false,
  false, true, true, true
};

// Down 7 = light jump right from any row
bool g_down_7[32] = {
  false, false, false, false,
  false, false, false, false,
  true, true, true, false,
  true, true, true, false,
  true, true, true, false,
  true, true, true, false,
  true, true, true, false,
  true, true, true, false
};

// Down 9 = light jump left from any row
bool g_down_9[32] = {
  false, false, false, false,
  false, false, false, false,
  false, true, true, true,
  false, true, true, true,
  false, true, true, true,
  false, true, true, true,
  false, true, true, true,
  false, true, true, true
};
#pragma endregion

} // anonymous namespace

Board::Board() {
  Reset();
}

Board::Board(Board const & other) {
  *this = other;
}

Board::~Board() {
}

void Board::Reset() {
  for (size_t i = 0; i < 12; ++i)
    board_[i] = kDarkSquare;
  for (size_t i = 12; i < 20; ++i)
    board_[i] = kEmptySquare;
  for (size_t i = 20; i < 32; ++i)
    board_[i] = kLightSquare;
}

SquareType Board::GetSquareAt(uint8_t square) const {
  CHECK_SQUARE(square);
  return board_[square-1];
}

void Board::SetSquareAt(uint8_t square, SquareType to) {
  CHECK_SQUARE(square);
  board_[square-1] = to;
}

uint8_t Board::GetSquareCount(SquareType type) const {
  uint8_t count = 0;
  for (uint8_t i = 0; i < 32; ++i) {
    if (board_[i] == type)
      ++count;
  }
  return count;
}

uint8_t Board::GetStuckSquareCount(SquareType type) const {
  uint8_t count = 0;
  if (type == kDarkSquare) {
    for (uint8_t i = 28; i < 32; ++i) {
      if (board_[i] == kDarkSquare)
        ++count;
    }
  } else if (type == kLightSquare) {
    for (uint8_t i = 0; i < 4; ++i) {
      if (board_[i] == kLightSquare)
        ++count;
    }
  }
  return count;
}

void Board::MakeMove(MoveType move, uint32_t & dark_captured, uint32_t & light_captured) {
  CHECK_SQUARE(move.from);
  CHECK_SQUARE(move.to);

  undo_.push(unique_ptr<Board>(new Board));
  *undo_.top() = *this;

  auto from_idx = move.from - 1;
  auto to_idx = move.to - 1;

  auto from_type = board_[from_idx];
  auto to_type = board_[to_idx];

  board_[from_idx] = kEmptySquare;
  board_[to_idx] = from_type;

  bool capture = IsJump(move);
  if (capture) {
    uint8_t capture_square = 0;
    bool even_row = from_idx % 8 < 4;

    if (to_idx == from_idx+7)
      capture_square = even_row ? move.from + 4 : move.from + 3;
    else if (to_idx == from_idx+9)
      capture_square = even_row ? move.from + 5 : move.from + 4;
    else if (to_idx == from_idx-7)
      capture_square = even_row ? move.from - 3 : move.from - 4;
    else if (to_idx == from_idx-9)
      capture_square = even_row ? move.from - 4 : move.from - 5;

    CHECK_SQUARE(capture_square);

    board_[capture_square-1] = kEmptySquare;

    if (from_type == kDarkSquare)
      ++light_captured;
    else
      ++dark_captured;
  }
}

void Board::UndoMove() {
  if (!undo_.empty()) {
    *this = *undo_.top();
    undo_.pop();
  }
}

bool Board::IsMoveLegal(MoveType move) const {
  CHECK_SQUARE(move.from);
  CHECK_SQUARE(move.to);

  if (board_[move.to-1] != kEmptySquare)
    return false;
  if (board_[move.from-1] == kEmptySquare)
    return false;

  auto moves = GetAvailableMoves(move.from);
  for (auto const & legal_move : moves) {
    if (move.to == legal_move.to)
      return true;
  }

  return false;
}

bool Board::IsJump(MoveType move) const {
  return move.to == move.from + 7 || move.to == move.from + 9 ||
         move.to == move.from - 7 || move.to == move.from - 9;
}

vector<MoveType> Board::GetAllAvailableMoves(SquareType player) const {
  assert(player != kEmptySquare);

  vector<MoveType> jumps;
  vector<MoveType> non_jumps;
  
  for (uint8_t i = 0; i < 32; ++i) {
    if (board_[i] == player) {
      auto moves = GetAvailableMoves(i+1);
      if (moves.size() > 0 && IsJump(moves[0]))
        jumps.insert(end(jumps), begin(moves), end(moves));
      else
        non_jumps.insert(end(non_jumps), begin(moves), end(moves));
    }
  }

  return jumps.size() > 0 ? jumps : non_jumps;
}

vector<MoveType> Board::GetAvailableMoves(uint8_t square) const {
  CHECK_SQUARE(square);

  vector<MoveType> moves;
  moves.reserve(2);

  auto square_idx = square - 1;
  auto square_type = board_[square_idx];
  bool even_row = square_idx % 8 < 4;

  // Check and add jumps first
  switch (square_type) {
    default: case kEmptySquare:
      break;

    case kDarkSquare:
      // Even rows + 7, capture = +4
      // Even rows + 9, capture = +5
      // Odd rows + 7, capture = +3
      // Odd rows + 9, capture = +4
      if (g_up_7[square_idx] && board_[square_idx+7] == kEmptySquare &&
          (even_row && board_[square_idx+4] == kLightSquare ||
           g_up_3[square_idx] && board_[square_idx+3] == kLightSquare)) {
        moves.push_back({square, square+7});
      }
      if (g_up_9[square_idx] && board_[square_idx+9] == kEmptySquare &&
          (!even_row && board_[square_idx+4] == kLightSquare ||
           g_up_5[square_idx] && board_[square_idx+5] == kLightSquare)) {
        moves.push_back({square, square+9});
      }

      // Only add non-jumps if there were no jumps added
      if (moves.empty()) {
        if (square_idx < 28 && board_[square_idx+4] == kEmptySquare)
          moves.push_back({square, square+4});
        if (g_up_3[square_idx] && board_[square_idx+3] == kEmptySquare)
          moves.push_back({square, square+3});
        if (g_up_5[square_idx] && board_[square_idx+5] == kEmptySquare)
          moves.push_back({square, square+5});
      }
      break;

    case kLightSquare:
      if (g_down_7[square_idx] && board_[square_idx-7] == kEmptySquare &&
          (!even_row && board_[square_idx-4] == kDarkSquare ||
           g_down_3[square_idx] && board_[square_idx-3] == kDarkSquare)) {
        moves.push_back({square, square-7});
      }
      if (g_down_9[square_idx] && board_[square_idx-9] == kEmptySquare &&
          (even_row && board_[square_idx-4] == kDarkSquare ||
           g_down_5[square_idx] && board_[square_idx-5] == kDarkSquare)) {
        moves.push_back({square, square-9});
      }

      if (moves.empty()) {
        if (square_idx >= 4 && board_[square_idx-4] == kEmptySquare)
          moves.push_back({square, square-4});
        if (g_down_3[square_idx] && board_[square_idx-3] == kEmptySquare)
          moves.push_back({square, square-3});
        if (g_down_5[square_idx] && board_[square_idx-5] == kEmptySquare)
          moves.push_back({square, square-5});
      }
      break;
  }

  return moves;
}

uint8_t Board::GetForwardProgessCount(SquareType player) const {
  CHECK_SQUARE(player);

  uint8_t result = 0;
  for (uint8_t i = 0; i < 32; ++i) {
    auto row = i % 4;
    if (board_[i] == player)
      result += (player == kDarkSquare ? 7 - row : row);
  }
  return result;
}

uint8_t Board::GetEdgePieceCount(SquareType player) const {
  CHECK_SQUARE(player);

  uint8_t result = 0;
  for (uint8_t i = 0; i < 32; ++i) {
    if (board_[i] == player && g_edge[i])
      ++result;
  }
  return result;
}

void Board::SetBoard(vector<uint8_t> squares) {
  assert(squares.size() == 32);
  for (size_t i = 0; i < squares.size(); ++i) {
    board_[i] = static_cast<SquareType>(squares[i]);
  }
}
Board & Board::operator=(Board const & other) {
  std::copy(other.board_, other.board_ + 32, board_);
  return *this;
}

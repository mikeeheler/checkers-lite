﻿#pragma once
#ifndef CHECKERS_LITE_CONSOLE_UI_H_
#define CHECKERS_LITE_CONSOLE_UI_H_ 1

#include <iostream>
#include "types.h"
#include "board.h"

class Game;

class ConsoleUI {
 public:
  explicit ConsoleUI(Game * game, std::ostream & out = std::cout, std::istream & in = std::cin);
  ~ConsoleUI();

  void Initialize();
  void Open();
  void Quit();

 private:
  void DrawPlayerName(SquareType player) const;
  void DrawHeader() const;
  void DrawGameBoard(Board const &, bool print_score = false) const;
  
  void DoMove(MoveType move);
  void DoAITurn();
  void ProcessInput();
  
  void DispatchCommand(string command, string params);

  void HandleHelpCommand(string params);
  void HandleShowCommand(string params);
  void HandleMoveCommand(string params);
  
  void PrintHelp() const;
  void PrintRules() const;
  void PrintMoveHelp() const;
  void PrintAvailableMoves(SquareType player) const;
  void PrintMoveHistory() const;

  Game * game_;
  std::ostream & out_;
  std::istream & in_;
};

#endif // CHECKERS_LITE_CONSOLE_UI_H_

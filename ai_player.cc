﻿#include "ai_player.h"
#include <cassert>
#include <iostream>
#include <limits>
#include <random>
#include "types.h"
#include "util.h"

using ::std::cout;
using ::std::numeric_limits;
using ::std::vector;

namespace {

struct GameNode {
  MoveType move;
  int32_t score;
  int32_t alpha;
  int32_t beta;
};

int32_t const kMaxInt = numeric_limits<int32_t>::max();
int32_t const kMinInt = numeric_limits<int32_t>::min();

std::random_device g_random_device;
std::mt19937 g_random_generator(g_random_device());

SquareType OtherSide(SquareType side) {
  return side == kDarkSquare ? kLightSquare : kDarkSquare;
}

int32_t EvaluateBoard(Board const & board, SquareType side, int32_t depth) {
  auto other_side = OtherSide(side);

  auto my_count = board.GetSquareCount(side) - board.GetStuckSquareCount(side);
  auto other_count = board.GetSquareCount(other_side) - board.GetStuckSquareCount(other_side);

  if (my_count == 0)
    return kMinInt + depth;
  if (other_count == 0)
    return kMaxInt - depth;

  auto my_moves = board.GetAllAvailableMoves(side).size();
  auto other_moves = board.GetAllAvailableMoves(other_side).size();

  if (my_moves != other_moves) {
    if (my_moves == 0)
      return kMinInt + depth;
    if (other_moves == 0)
      return kMaxInt - depth;
  }

  auto my_forward = board.GetForwardProgessCount(side);
  auto other_forward = board.GetForwardProgessCount(other_side);

  auto my_edges = board.GetEdgePieceCount(side);
  auto other_edges = board.GetEdgePieceCount(other_side);
  
  return ((my_count - my_edges) * 100 - (other_count - other_edges) * 100) +
         (my_forward * 75 - other_forward * 75) +
         (my_edges * 80 - other_edges * 80)
         - depth;
}

int32_t FindMoves(GameNode * node, Board & board, SquareType side, int32_t search_depth, int32_t level, int32_t alpha, int32_t beta, bool maximizing) {
  // Continue forced jumps if we just jumped and there are jumps available from
  // the landed square.
  if (node->move.from != 255 && board.IsJump(node->move)) {
    auto next_moves = board.GetAvailableMoves(node->move.to);
    if (next_moves.size() > 0 && board.IsJump(next_moves[0])) {
      maximizing = !maximizing;
      ++search_depth;
    }
  }

  auto moves = board.GetAllAvailableMoves(maximizing ? side : OtherSide(side));

  // We've gone too far, just pick a random move.
  if (search_depth < 0)
    return kMaxInt;

  node->alpha = alpha;
  node->beta = beta;

  if (moves.empty() || search_depth <= 0) {
    node->score = EvaluateBoard(board, side, level);
    return node->score;
  }

  for (auto const & move : moves) {
    GameNode child_node;
    child_node.move = move;

    uint32_t captured = 0;
    board.MakeMove(move, captured, captured);
    auto score = FindMoves(&child_node, board, side, search_depth-1, level+1,
                           node->alpha, node->beta, !maximizing);
    board.UndoMove();

    if (maximizing) {
      if (score > node->alpha) node->alpha = score;
    } else if (score < node->beta) {
      node->beta = score;
    }
    
    if (node->beta <= node->alpha)
      break;
  }

  node->score = maximizing ? node->alpha : node->beta;

  return node->score;
}

} // anonymous namespace

AIPlayer::AIPlayer(AISkillLevel skill)
    : skill_(skill) {
}

AIPlayer::~AIPlayer() {
}

MoveType AIPlayer::GetMove(Board const & board, SquareType side) {
  assert(side != kEmptySquare);

  auto moves = board.GetAllAvailableMoves(side);
  if (moves.size() == 1)
    return moves[0];

  bool maximizing = (skill_ == kLetMeWin ? false : true);
  int32_t depth = 0;

  switch (skill_) {
    case kLetMeWin: depth = 7; break; // We won't try _too_ hard
    case kRandom: depth = -1; break;
    case kEasy: depth = 5; break;
    case kMedium: depth = 7; break;
    case kHard: depth = 11; break;
  }

  int32_t best_score = kMinInt;
  vector<int32_t> scores(moves.size(), kMinInt);

  GameNode node;
  node.alpha = kMinInt;
  node.beta = kMaxInt;
  node.score = 0;

  for (size_t i = 0; i < moves.size(); ++i) {
    auto const & move = moves[i];
    node.move = move;

    Board search_board = {board};
    uint32_t captured;
    search_board.MakeMove(move, captured, captured);

    scores[i] = FindMoves(&node, search_board, side, depth, 0, node.alpha, node.beta, maximizing);
    if (scores[i] > best_score)
      best_score = scores[i];
  }

  vector<MoveType> best_moves;

  if (best_score < kMinInt / 2) {
    // All we can see is the end
    size_t worst_move_index = 0;
    for (size_t i = 1; i < moves.size(); ++i) {
      if (scores[i] < scores[worst_move_index])
        worst_move_index = i;
    }
    best_score = scores[worst_move_index];
  }

  for (size_t i = 0; i < moves.size(); ++i) {
    if (scores[i] == best_score) {
      best_moves.push_back(moves[i]);
    }
  }

  assert(!best_moves.empty());

  std::uniform_int_distribution<size_t> random_move(0, best_moves.size()-1);
  return best_moves[random_move(g_random_generator)];
}

void AIPlayer::SetSkillLevel(AISkillLevel skill) {
  skill_ = skill;
}

﻿#pragma once
#ifndef CHECKERS_LITE_TYPES_H_
#define CHECKERS_LITE_TYPES_H_ 1

// Common STL types
#include <cstdint>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

using ::std::make_shared;
using ::std::shared_ptr;
using ::std::string;
using ::std::unique_ptr;
using ::std::vector;
using ::std::weak_ptr;

#endif // CHECKERS_LITE_TYPES_H_

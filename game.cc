﻿#include "game.h"
#include <cassert>
#include <iostream>
#include <random>
#include <sstream>
#include "console_ui.h"
#include "util.h"

using ::std::vector;

namespace {

std::random_device g_random_device;
std::mt19937 g_random_generator(g_random_device());

}

InvalidMoveException::InvalidMoveException(string const & what) : std::invalid_argument(what) {}
InvalidMoveException::InvalidMoveException(char const * what) : std::invalid_argument(what) {}

Game::Game()
    : ui_(this),
      game_state_(kStartup),
      dark_captured_(0),
      light_captured_(0),
      current_player_(kDarkSquare) {
  std::uniform_int_distribution<int> rand(0, 1);

  players_[0] = static_cast<PlayerType>(rand(g_random_generator));
  players_[1] = static_cast<PlayerType>(1 - static_cast<int>(players_[0]));

  //players_[0] = kAIPlayer;
  //players_[1] = kAIPlayer;

  std::uniform_int_distribution<size_t> skill_rand(0, 4);
  ai_skill_[0] = static_cast<AISkillLevel>(skill_rand(g_random_generator));
  ai_skill_[1] = static_cast<AISkillLevel>(skill_rand(g_random_generator));

  //ai_skill_[0] = kEasy;
  //ai_skill_[1] = kHard;

  ai_.SetSkillLevel(GetPlayerSkill(current_player_));
}

Game::~Game() {
}

void Game::Initialize(int argc, char * argv[]) {
  board_.Reset();
  ui_.Initialize();
  move_history_.clear();
}

void Game::Run() {
  game_state_ = kActive;
  ui_.Open();
  game_state_ = kPostGame;
}

void Game::Shutdown() {
  game_state_ = kShutdown;
}

bool Game::IsRunning() const {
  return game_state_ == kActive;
}

void Game::MakeMove(MoveType move) {
  auto from_type = board_.GetSquareAt(move.from);
  auto to_type = board_.GetSquareAt(move.to);

  std::stringstream move_ss;
  move_ss << static_cast<int>(move.from) << '-' << static_cast<int>(move.to);

  if (from_type != current_player_)
    throw InvalidMoveException(move_ss.str());
  if (from_type == kEmptySquare)
    throw InvalidMoveException(move_ss.str());
  if (to_type != kEmptySquare)
    throw InvalidMoveException(move_ss.str());
  if (!board_.IsMoveLegal(move))
    throw InvalidMoveException(move_ss.str());

  uint32_t dark_captured = 0;
  uint32_t light_captured = 0;
  board_.MakeMove(move, dark_captured, light_captured);

  dark_captured_ += dark_captured;
  light_captured_ += light_captured;

  move_history_.push_back({from_type, move, (dark_captured + light_captured > 0)});
}

void Game::AdvanceTurn() {
  auto last_player = current_player_;
  current_player_ = (current_player_ == kDarkSquare ? kLightSquare : kDarkSquare);
  ai_.SetSkillLevel(GetPlayerSkill(current_player_));
  
  // Test if the game is over
  // The game ends when the active player cannot move. If the opponent still
  // can move then the opponent wins. Otherwise the player with the fewest
  // captured pieces wins.
  auto moves = board_.GetAllAvailableMoves(current_player_);
  if (moves.empty()) {
    auto opponent_moves = board_.GetAllAvailableMoves(last_player);
    
    // Neither side can move, compare captured pieces.
    if (opponent_moves.empty()) {
      // Stalemate
      if (dark_captured_ > light_captured_)
        game_state_ = kLightWins;
      else if (light_captured_ > dark_captured_)
        game_state_ = kDarkWins;
      else
        game_state_ = kStalemate;
    } else if (current_player_ == kDarkSquare) {
      game_state_ = kLightWins;
    } else if (current_player_ == kLightSquare) {
      game_state_ = kDarkWins;
    } else {
      assert(false && "Unhandled value for current_player_");
    }
  }
}

AIPlayer & Game::GetAIPlayer() {
  return ai_;
}

Board & Game::GetBoard() {
  return board_;
}

ConsoleUI & Game::GetUI() {
  return ui_;
}

GameState Game::GetGameState() const {
  return game_state_;
}

uint32_t Game::GetDarkCapturedCount() const {
  return dark_captured_;
}

uint32_t Game::GetLightCapturedCount() const {
  return light_captured_;
}

SquareType Game::GetCurrentPlayer() const {
  return current_player_;
}

AISkillLevel Game::GetPlayerSkill(SquareType player) const {
  assert(player != kEmptySquare);
  return ai_skill_[static_cast<size_t>(player)-1];
}

string Game::GetPlayerName(SquareType player) const {
  assert(player != kEmptySquare);
  return player == kDarkSquare ? "Dark" : "Light";
}

PlayerType Game::GetPlayerType(SquareType player) const {
  assert(player != kEmptySquare);
  return player == kDarkSquare ? players_[0] : players_[1];
}

vector<MoveHistoryType> Game::GetMoveHistory() const {
  return move_history_;
}

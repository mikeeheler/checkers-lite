﻿@echo off
call "%VS120COMNTOOLS%\..\..\VC\vcvarsall.bat" x86
cd /D "%~dp0"
set SOURCE_DIR=%CD%
set BUILD_DIR=%CD%\build
set PATH=%SOURCE_DIR%\cmake\bin;%PATH%
cmake --version
if not exist "%BUILD_DIR%" (
  cmake -E make_directory "%BUILD_DIR%"
  cd /D "%BUILD_DIR%"
  cmake -G "Visual Studio 12" -D CMAKE_CONFIGURATION_TYPES:STRING="Debug;Release" "%SOURCE_DIR%"
) else (
  cd /D "%BUILD_DIR%"
  cmake .
)
start checkers_lite.sln

﻿#include "console_ui.h"
#include <algorithm>
#include <iomanip>
#include <regex>
#include <sstream>
#include <windows.h>
#include "board.h"
#include "game.h"
#include "types.h"
#include "util.h"
#include "version.h"

using ::std::regex;
using ::std::stringstream;

enum ConsoleColor {
  kBlack = 0, kDarkBlue, kDarkGreen, kDarkTeal, kDarkRed, kDarkMagenta, kDarkYellow, kGray,
  kDarkGray, kBlue, kGreen, kTeal, kRed, kMagenta, kYellow, kWhite
};

namespace {

regex const g_command_regex("\\s*([a-zA-z\\?]+)\\s*([\\w\\d-]*)");
regex const g_common_params_regex("\\s*([\\w]+)");
vector<string> const g_player_type_string = {"Human", "AI"};
vector<string> const g_skill_string = {"YouWin", "Random", "Easy", "Medium", "Hard"};

void SetConsoleColor(ConsoleColor color) {
  static HANDLE handle = nullptr;
  if (handle == nullptr)
    handle = GetStdHandle(STD_OUTPUT_HANDLE);
  SetConsoleTextAttribute(handle, static_cast<WORD>(color));
}

} // anonymous namespace

ConsoleUI::ConsoleUI(Game * game, std::ostream & out, std::istream & in)
    : game_(game),
      out_(out),
      in_(in) {
  assert(game_ != nullptr);
}

ConsoleUI::~ConsoleUI() {
}

void ConsoleUI::Initialize() {

}

void ConsoleUI::Open() {
  SetConsoleColor(kGray);
  DrawHeader();
  DrawGameBoard(game_->GetBoard(), true);
  out_ << '\n';
  
  while (game_->IsRunning()) {
    if (game_->GetPlayerType(game_->GetCurrentPlayer()) == kAIPlayer) {
      DoAITurn();
    } else {
      ProcessInput();
    }

    out_ << '\n';
  }

  SetConsoleColor(kGray);
  out_ << "Game Over! ";

  switch (game_->GetGameState()) {
    case kDarkWins:
      SetConsoleColor(kDarkGray);
      out_ << "Dark Wins!";
      break;

    case kLightWins:
      SetConsoleColor(kWhite);
      out_ << "Light Wins!";
      break;

    case kStalemate:
      SetConsoleColor(kRed);
      out_ << "It's a Draw!";
      break;
  }

  SetConsoleColor(kGray);
  out_ << "\n\n";

  while (game_->GetGameState() != kShutdown) {
    ProcessInput();
    out_ << '\n';
  }
}

void ConsoleUI::Quit() {
  SetConsoleColor(kWhite);
  out_ << "Shutting Down...\n\n";
  SetConsoleColor(kGray);
  game_->Shutdown();
}

void ConsoleUI::DrawPlayerName(SquareType player) const {
  assert(player != kEmptySquare);
  SetConsoleColor(player == kDarkSquare ? kDarkGray : kWhite);
  out_ << game_->GetPlayerName(player);
}

void ConsoleUI::DrawHeader() const {
  SetConsoleColor(kDarkGray);
  out_ << "------------------------------------------------------------------------\n";

  SetConsoleColor(kWhite);
  out_ << " Checkers Lite";
  
  SetConsoleColor(kGray);
  out_ << "                                              Mike Eheler\n";
  out_ << " Version ";
  out_ << std::setw(11) << std::setiosflags(std::ios::left) << VERSION_STRING;
  out_ << std::resetiosflags(std::ios::adjustfield);
  out_ << "                            <mike.eheler@gmail.com>\n";

  SetConsoleColor(kDarkGray);
  out_ << "------------------------------------------------------------------------\n\n";

  SetConsoleColor(kGray);
}

void ConsoleUI::DrawGameBoard(Board const & board, bool print_score) const {
  for (size_t i = 0; i < 32; ++i) {
    SetConsoleColor(kGray);

    auto row = i / 4;
    if (i % 4 == 0) out_ << "  ";
    if (i % 8 < 4) out_ << " .";

    switch (board.GetSquareAt(static_cast<uint8_t>(i+1))) {
      default: case kEmptySquare:
        SetConsoleColor(kGray);
        out_ << " .";
        break;
      case kDarkSquare:
        SetConsoleColor(kDarkGray);
        out_ << " X";
        break;
      case kLightSquare:
        SetConsoleColor(kWhite);
        out_ << " O";
        break;
    }

    SetConsoleColor(kGray);

    if (i % 8 >= 4) out_ << " .";
    if (i % 4 == 3) {
      if (print_score) {
        SetConsoleColor(kDarkGray);
        out_ << "  | ";
        SetConsoleColor(kGray);

        if (row == 0) out_ << "Captured:";
        if (row == 1) {
          out_ << "  ";
          DrawPlayerName(kDarkSquare);
          out_ << "   ";
          SetConsoleColor(kGray);
          out_ << std::setw(2) << game_->GetDarkCapturedCount();
          out_ << "  (" << g_player_type_string[static_cast<size_t>(game_->GetPlayerType(kDarkSquare))];
          if (game_->GetPlayerType(kDarkSquare) == kAIPlayer) {
            out_ << '-' << g_skill_string[static_cast<size_t>(game_->GetPlayerSkill(kDarkSquare))];
          }
          out_ << ')';
        }
        if (row == 2) {
          out_ << "  ";
          DrawPlayerName(kLightSquare);
          out_ << "  ";
          SetConsoleColor(kGray);
          out_ << std::setw(2) << game_->GetLightCapturedCount();
          out_ << "  (" << g_player_type_string[static_cast<size_t>(game_->GetPlayerType(kLightSquare))];
          if (game_->GetPlayerType(kLightSquare) == kAIPlayer) {
            out_ << '-' << g_skill_string[static_cast<size_t>(game_->GetPlayerSkill(kLightSquare))];
          }
          out_ << ')';
        }
        if (row == 7) {
          switch (game_->GetGameState()) {
            case kActive:
              DrawPlayerName(game_->GetCurrentPlayer());
              out_ << "'s Turn";
              break;

            case kDarkWins:
              DrawPlayerName(kDarkSquare);
              out_ << " Wins!";
              break;

            case kLightWins:
              DrawPlayerName(kLightSquare);
              out_ << " Wins!";
              break;

            case kStalemate:
              SetConsoleColor(kRed);
              out_ << "Draw";
              break;
          }
          SetConsoleColor(kGray);
        }
      }
      out_ << "\n";
    }
  }
}

void ConsoleUI::DoMove(MoveType move) {
  auto const & board = game_->GetBoard();
  bool jump_move = board.IsJump(move);
  bool advance_turn = true;

  game_->MakeMove(move);
  out_ << "-> Move: " << static_cast<int>(move.from) << (jump_move ? 'x' : '-') << static_cast<int>(move.to) << '\n';

  while (jump_move) {
    auto next_moves = board.GetAvailableMoves(move.to);

    if (next_moves.empty() || !board.IsJump(next_moves[0]))
      break;
    
    if (next_moves.size() != 1) {
      advance_turn = false;
      break;
    }
    
    move = next_moves[0];
    game_->MakeMove(move);
    jump_move = board.IsJump(move);

    out_ << "-> Forced jump: " << static_cast<int>(move.from) << 'x' << static_cast<int>(move.to) << '\n';
  }
      
  if (advance_turn)
    game_->AdvanceTurn();
  else if (game_->GetPlayerType(game_->GetCurrentPlayer()) != kAIPlayer)
    out_ << "!! Multiple jumps available. Go again...\n";
  out_ << '\n';
    
  //DrawHeader();
  DrawGameBoard(game_->GetBoard(), true);
}

void ConsoleUI::DoAITurn() {
  out_ << "AI is computing a move..." << std::flush;
  auto move = game_->GetAIPlayer().GetMove(game_->GetBoard(), game_->GetCurrentPlayer());
  out_ << "\n\n";

  DoMove(move);
}

void ConsoleUI::ProcessInput() {
  string input;
  
  out_ << "Input (? for help): ";
  if (!std::getline(in_, input)) {
    // User pressed ctrl+z
    Quit();
    return;
  }
  out_ << '\n';

  std::smatch matches;
  if (std::regex_match(input, matches, g_command_regex)) {
    string command;
    command = matches[1].str();
    std::transform(begin(command), end(command), begin(command), ::tolower);

    try {
      DispatchCommand(command, matches[2].str());
    } catch (std::invalid_argument) {
      SetConsoleColor(kRed);
      out_ << "Invalid input: ";
      SetConsoleColor(kGray);
      out_ << input << '\n';
    }
  }

  out_ << '\n';
}

void ConsoleUI::DispatchCommand(string command, string params) {
  std::smatch matches;
  if (command == "quit" || command == "q")
    Quit();
  else if (command == "?" || command == "h" || command == "help")
    HandleHelpCommand(params);
  else if (command == "show" || command == "s")
    HandleShowCommand(params);
  else if (command == "move" || command == "m")
    HandleMoveCommand(params);
  else
    throw std::invalid_argument(command);
}

void ConsoleUI::HandleHelpCommand(string params) {
  std::smatch matches;
  if (std::regex_match(params, matches, g_common_params_regex) && !matches[1].str().empty()) {
    auto help_command = matches[1].str();
    if (help_command == "rules")
      PrintRules();
    else if (help_command == "moves")
      PrintMoveHelp();
    else
      PrintHelp();
  } else {
    PrintHelp();
  }
}

void ConsoleUI::HandleShowCommand(string params) {
  std::smatch matches;
  if (std::regex_match(params, matches, g_common_params_regex)) {
    auto show_command = matches[1].str();
    if (show_command == "board")
      DrawGameBoard(game_->GetBoard(), true);
    else if (show_command == "moves")
      PrintAvailableMoves(game_->GetCurrentPlayer());
    else if (show_command == "history")
      PrintMoveHistory();
  }
}

void ConsoleUI::HandleMoveCommand(string params) {
  MoveType move;

  std::regex standard_regex("([0-9]{1,2})-([0-9]{1,2})");
  std::regex algebraic_regex("([a-h])([1-8])\\s*([a-h])([1-8])");
  std::smatch matches;

  if (std::regex_match(params, matches, standard_regex)) {
    auto from = stol(matches[1].str());
    auto to = stol(matches[2].str());
    
    if (from < 1 || from > 32)
      throw std::invalid_argument(matches[1].str());
    if (to < 1 || to > 32)
      throw std::invalid_argument(matches[2].str());

    move.from = static_cast<uint8_t>(from);
    move.to = static_cast<uint8_t>(to);
  } else if (std::regex_match(params, matches, algebraic_regex)) {
    int32_t from_col = 7 - (matches[1].str().at(0) - 'a');
    int32_t from_row = stol(matches[2].str()) - 1;
    int32_t to_col = 7 - (matches[3].str().at(0) - 'a');
    int32_t to_row = stol(matches[4].str()) - 1;

    // Validation: Even columns on odd rows and vice versa
    if (from_row % 2 == from_col % 2)
      throw std::invalid_argument(matches[1].str() + matches[2].str());
    if (to_row % 2 == to_col % 2)
      throw std::invalid_argument(matches[3].str() + matches[4].str());

    // We can get away with this because of integer floor rounding
    move.from = static_cast<uint8_t>(from_row * 4 + from_col / 2 + 1);
    move.to = static_cast<uint8_t>(to_row * 4 + to_col / 2 + 1);
  } else {
    throw std::invalid_argument(params);
  }

  try {
    DoMove(move);
  } catch (InvalidMoveException const & e) {
    SetConsoleColor(kRed);
    out_ << "Invalid move: ";
    SetConsoleColor(kGray);
    out_ << e.what() << '\n';
  }
}

void ConsoleUI::PrintHelp() const {
  SetConsoleColor(kWhite);
  out_ << "Command Help\n\n";

  SetConsoleColor(kGray);
  out_ << "quit               Exits Checkers Lite\n";
  out_ << "help               Prints this help screen\n";
  out_ << "help rules         Prints the rules of Checkers Lite\n";
  out_ << "help moves         Prints help about making moves\n";
  out_ << "show board         Shows the game board\n";
  out_ << "show moves         Shows a list of your available moves\n";
  out_ << "show history       Shows a list of the moves played so far\n";
  out_ << "move A-B           Move your piece from square A to square B\n";
}

void ConsoleUI::PrintRules() const {
  SetConsoleColor(kWhite);
  out_ << "Rules for Checkers Lite\n\n";

  SetConsoleColor(kGray);
  out_ << "Checkers Lite is like a standard game of Checkers (or Draughts) with the\n";
  out_ << "exception that there are no kings.\n\n";

  out_ << "Pieces are positioned with the dark pieces on top and light pieces on\n";
  out_ << "the bottom. Dark moves first. Moves are made by moving a piece to an\n";
  out_ << "empty square toward your opponent and diagonally either to the right or\n";
  out_ << "left.\n\n";

  out_ << "If an opponent's piece is in one of these squares, you must jump it\n";
  out_ << "which will capture the piece and remove it from play. When a jump is\n";
  out_ << "available, it must be taken. If more than one jump is available in a\n";
  out_ << "turn you may chose which jump to take. The piece you jumped with must\n";
  out_ << "continue until all jumps have been taken.\n";
}

void ConsoleUI::PrintMoveHelp() const {
  SetConsoleColor(kWhite);
  out_ << "How to Make Moves\n\n";

  SetConsoleColor(kGray);
  out_ << "The board is represented by 32 legal squares:\n\n";
  
  SetConsoleColor(kWhite);
  out_ << "     Standard Notation    ";
  SetConsoleColor(kGray);
  out_ << '|';
  SetConsoleColor(kWhite);
  out_ << "    Algebraic Notation\n";
  SetConsoleColor(kGray);

  out_ << "                          |\n";
  out_ << "      1     2     3    4  |     g1    e1    c1    a1\n";
  out_ << "   5     6     7     8    |  h2    f2    d2    b2   \n";
  out_ << "      9    10    11   12  |     g3    e3    c3    a3\n";
  out_ << "  13    14    15    16    |  h4    f4    d4    b4   \n";
  out_ << "     17    18    19   20  |     g5    e5    c5    a5\n";
  out_ << "  21    22    23    24    |  h6    f6    d6    b6   \n";
  out_ << "     25    26    27   28  |     g7    e7    c7    a7\n";
  out_ << "  29    30    31    32    |  h8    f8    d8    b8   \n";
  out_ << '\n';

  out_ << "You make a move by using the move command. Indicate your move by using\n";
  out_ << "the notation source-target. For example: move 10-14 will move a piece\n";
  out_ << "from square 10 to square 14.\n\n";

  out_ << "You may also use algebraic notation which may be more familiar to chess\n";
  out_ << "players. In this case use \"move e3f4\" to make the same move from 10\n";
  out_ << "to 14. Note: Be aware of the hidden squares. For example a move to a8 is\n";
  out_ << "never valid in checkers.\n";
}

void ConsoleUI::PrintAvailableMoves(SquareType player) const {
  assert(player != kEmptySquare);
  
  auto moves = game_->GetBoard().GetAllAvailableMoves(player);
  
  if (moves.empty()) {
    SetConsoleColor(player == kDarkSquare ? kDarkGray : kGray);
    out_ << game_->GetPlayerName(player);
    SetConsoleColor(kGray);
    out_ << " has no available moves.\n";
    return;
  }

  SetConsoleColor(player == kDarkSquare ? kDarkGray : kGray);
  out_ << game_->GetPlayerName(player) << "'s ";
  SetConsoleColor(kGray);
  out_ << "Available Moves:\n\n";

  for (auto const & move : moves) {
    out_ << "  " << static_cast<int>(move.from) << '-' << static_cast<int>(move.to);
    if (game_->GetBoard().IsJump(move)) {
      SetConsoleColor(kWhite);
      out_ << " (Capture)";
      SetConsoleColor(kGray);
    }
    out_ << '\n';
  }
}

void ConsoleUI::PrintMoveHistory() const {
  auto moves = game_->GetMoveHistory();
  if (moves.empty()) {
    out_ << "There are no moves to show.\n";
    return;
  }

  stringstream ss;
  ss << moves.size();
  size_t padding = ss.str().length() + 2;

  SetConsoleColor(kWhite);
  out_ << "The Game So Far:\n\n";

  size_t i = 1;
  auto last_player = kEmptySquare;
  for (auto const & move : moves) {
    if (last_player != kEmptySquare && last_player != move.player)
      out_ << '\n';

    // Handle forced jumps with 1x2x3 notation
    if (move.player != last_player) {
      SetConsoleColor(kGray);
      out_ << std::setw(padding) << i << ". ";

      SetConsoleColor(move.player == kDarkSquare ? kDarkGray : kWhite);
      out_ << std::setw(0) << static_cast<int>(move.move.from);
      ++i;
    }

    SetConsoleColor(move.player == kDarkSquare ? kDarkGray : kWhite);
    out_ << (move.capture ? 'x' : '-');
    out_ << static_cast<uint32_t>(move.move.to);

    last_player = move.player;
  }

  SetConsoleColor(kGray);
  out_ << '\n';
}

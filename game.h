﻿#pragma once
#ifndef CHECKERS_LITE_GAME_H_
#define CHECKERS_LITE_GAME_H_ 1

#include <map>
#include "ai_player.h"
#include "board.h"
#include "console_ui.h"
#include "types.h"

enum PlayerType {
  kHumanPlayer,
  kAIPlayer
};

enum GameState {
  kStartup,
  kActive,
  kPostGame,
  kShutdown,
  kDarkWins,
  kLightWins,
  kStalemate
};

class InvalidMoveException : public std::invalid_argument {
 public:
  explicit InvalidMoveException(string const &);
  explicit InvalidMoveException(char const *);
};

struct MoveHistoryType {
  SquareType player;
  MoveType move;
  bool capture;
};

class Game {
 public:
  Game();
  ~Game();

  void Initialize(int argc, char * argv[]);
  void Run();
  void Shutdown();

  bool IsRunning() const;

  void MakeMove(MoveType move);
  void AdvanceTurn();

  AIPlayer & GetAIPlayer();
  Board & GetBoard();
  ConsoleUI & GetUI();

  GameState GetGameState() const;
  
  uint32_t GetDarkCapturedCount() const;
  uint32_t GetLightCapturedCount() const;

  SquareType GetCurrentPlayer() const;
  AISkillLevel GetPlayerSkill(SquareType player) const;
  string GetPlayerName(SquareType player) const;
  PlayerType GetPlayerType(SquareType player) const;

  std::vector<MoveHistoryType> GetMoveHistory() const;

 private:
  AIPlayer ai_;
  Board board_;
  ConsoleUI ui_;

  GameState game_state_;
  uint32_t dark_captured_;
  uint32_t light_captured_;
  SquareType current_player_;
  std::vector<MoveHistoryType> move_history_;
  PlayerType players_[2];
  AISkillLevel ai_skill_[2];
};

#endif // CHECKERS_LITE_GAME_H_

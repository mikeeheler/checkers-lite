﻿#pragma once
#ifndef CHECKERS_LITE_AI_PLAYER_H_
#define CHECKERS_LITE_AI_PLAYER_H_ 1

#include "board.h"

enum AISkillLevel {
  kLetMeWin,
  kRandom,
  kEasy,
  kMedium,
  kHard
};

struct AIPlayer {
 public:
  explicit AIPlayer(AISkillLevel skill = kHard);
  ~AIPlayer();

  MoveType GetMove(Board const &, SquareType side);
  void SetSkillLevel(AISkillLevel skill);

 private:
  AISkillLevel skill_;
};

#endif // CHECKERS_LITE_AI_PLAYER_H_
